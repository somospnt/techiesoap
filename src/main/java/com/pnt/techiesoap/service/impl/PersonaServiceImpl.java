package com.pnt.techiesoap.service.impl;

import com.pnt.techiesoap.generated.wsdl.LibroRequest;
import com.pnt.techiesoap.generated.wsdl.PersonaRequest;
import com.pnt.techiesoap.generated.wsdl.PersonaResponse;
import com.pnt.techiesoap.repository.PersonaRepository;
import com.pnt.techiesoap.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaServiceImpl implements PersonaService{
    
    @Autowired
    PersonaRepository personaRepository;
            
    @Override
    public PersonaResponse obtenerPersona(String dni) {
        PersonaRequest personaRequest = new PersonaRequest();
        personaRequest.setDNI(dni);
        
        PersonaResponse personaResponse = personaRepository.enviarConsulta(personaRequest);
        return personaResponse;
    }

    @Override
    public PersonaResponse obtenerPersonaPorLibro(String libroId) {
        return null;
    }
    
}
