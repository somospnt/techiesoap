package com.pnt.techiesoap.service;

import com.pnt.techiesoap.generated.wsdl.PersonaResponse;

public interface PersonaService {

    PersonaResponse obtenerPersona(String dni);
    
    PersonaResponse obtenerPersonaPorLibro(String libroId);

}
