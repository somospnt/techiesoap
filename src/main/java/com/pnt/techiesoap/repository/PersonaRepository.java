package com.pnt.techiesoap.repository;

import com.pnt.techiesoap.generated.wsdl.LibroRequest;
import com.pnt.techiesoap.generated.wsdl.PersonaRequest;
import com.pnt.techiesoap.generated.wsdl.PersonaResponse;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;


public class PersonaRepository extends WebServiceGatewaySupport {

    public PersonaResponse enviarConsulta(PersonaRequest personaRequest) {

        WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
        PersonaResponse personaResponse = (PersonaResponse) webServiceTemplate.marshalSendAndReceive(personaRequest);
        return personaResponse;

    }

}
