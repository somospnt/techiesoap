package com.pnt.techiesoap;

import com.pnt.techiesoap.repository.PersonaRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.config.annotation.EnableWs;

@SpringBootApplication
@EnableWs
public class TechieConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPaths("com.pnt.techiesoap.generated.wsdl");

        return marshaller;
    }

    @Bean
    public PersonaRepository personaRepository(Jaxb2Marshaller marshaller) {
        PersonaRepository client = new PersonaRepository();
        client.setDefaultUri("http://localhost:8088/TechieService?WSDL");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
