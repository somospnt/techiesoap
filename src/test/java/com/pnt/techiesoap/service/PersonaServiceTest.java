package com.pnt.techiesoap.service;

import com.pnt.techiesoap.TechieConfig;
import com.pnt.techiesoap.generated.wsdl.Libros;
import com.pnt.techiesoap.generated.wsdl.PersonaResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TechieConfig.class)
public class PersonaServiceTest {

    @Autowired
    PersonaService personaService;

    @Test
    public void obtenerPersona_personaConLibros_retornaPersonaConLibros() {
        String dni = "35000000";
        PersonaResponse personaResponse = personaService.obtenerPersona(dni);

        Assert.assertEquals(2, personaResponse.getNombre().size());
        Assert.assertEquals("Raul", personaResponse.getNombre().get(0));
        Assert.assertEquals("Jose", personaResponse.getNombre().get(1));
        Assert.assertEquals("Connectis", personaResponse.getApellido());
        Assert.assertEquals(2, personaResponse.getLibros().size());

        logPersona(personaResponse);
    }

    @Test
    public void obtenerPersonaPorLibro_personaTieneLibro_retornaPersonaConLibros() {
        String libroId = "008";
        PersonaResponse personaResponse = personaService.obtenerPersonaPorLibro(libroId);

        Assert.assertEquals(1, personaResponse.getNombre().size());
        Assert.assertEquals("Carlos", personaResponse.getNombre().get(0));
        Assert.assertEquals("Thales", personaResponse.getApellido());
        Assert.assertEquals(1, personaResponse.getLibros().size());

        logPersona(personaResponse);

    }

    private void logPersona(PersonaResponse personaResponse) {
        System.out.println("****************************************************");
        System.out.println("Id = " + personaResponse.getId());
        System.out.println("1° Nombre= " + personaResponse.getNombre().get(0));
        if (personaResponse.getNombre().size() == 2) {
            System.out.println("2° Nombre = " + personaResponse.getNombre().get(1));
        }
        System.out.println("Apellido = " + personaResponse.getApellido());
        for (Libros libro : personaResponse.getLibros()) {
            System.out.println("ID :" + libro.getID() + " -> Nombre: " + libro.getName());
        }
        System.out.println("****************************************************");
    }

}
